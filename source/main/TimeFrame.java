package sport.source.main;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;

public class TimeFrame extends JFrame{
	private static String nowTime = null;

	TimeFrame(){
		setTitle("時刻");
		setSize(400, 200);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);

		JPanel panel1 = new JPanel();
		ShowTimer timer = new ShowTimer();

		InputGameContents incon = new InputGameContents("sport/source/main/test.csv");
		String[][] game = incon.getGame();

		for (String[] str : game) {
			for (String element : str) {
				System.out.print(element);
			}
			System.out.println();
		}

		System.out.println(game[0][0]);

		panel1.add(timer);
		add(panel1, BorderLayout.NORTH);

	}
}