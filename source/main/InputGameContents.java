package sport.source.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class InputGameContents{

	private ArrayList<String[]> al = new ArrayList<String[]>();
	private static String[][] array;

	InputGameContents(String filename){

		try {
			File csv = new File(filename);
			FileInputStream input = new FileInputStream(csv);
			InputStreamReader stream = new InputStreamReader(input, "UTF-8");
			BufferedReader brf = new BufferedReader(stream);

			//バッファーをArrayListに１行ずつ カンマ 区切りで入れる
			while(brf.ready()) {
				String line = brf.readLine();
				this.al.add(line.split(","));
			}

			//配列を生成
			array = new String[this.al.size()][];
			for (int i = 0; i < this.al.size(); i++) {
				array[i] = this.al.get(i);
			}
			array[0][0] = "9:40";

			brf.close();

		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	String[][] getGame(){
		return array;
	}
}