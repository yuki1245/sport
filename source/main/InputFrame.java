package sport.source.main;

import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class InputFrame extends JFrame implements ActionListener{

	private String nowSelectGame;
	private String nowSelectGameNo;
	JComboBox<String> combo1;
	JComboBox<String> combo2;

	InputFrame(){
		setTitle("入力画面");
		setSize(600, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);

		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();

		JLabel label1 = new JLabel("入力画面");

		String whatSport[] = {"ソフト", "サッカー", "バレー", "バスケットボール"};
		combo1 = new JComboBox<String>(whatSport);
		combo1.addActionListener(this);
		combo1.setActionCommand("コンボ1");

		String gameNo[] = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15"};

		combo2 = new JComboBox(gameNo);
		combo2.addActionListener(this);
		combo2.setActionCommand("コンボ2");

		panel1.add(label1);

		panel2.add(combo1);
		panel2.add(combo2);

		add(panel1, BorderLayout.NORTH);
		add(panel2, BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent e){
		String cmd = e.getActionCommand();

		if (cmd.equals("コンボ1")) {
			System.out.println("コンボ1");
			nowSelectGame = (String)combo1.getSelectedItem();
		}else if(cmd.equals("コンボ2")){
			System.out.println("コンボ2");
			nowSelectGameNo = (String)combo2.getSelectedItem();
		}

		System.out.println(nowSelectGame);
		System.out.println(nowSelectGameNo);

	}
}