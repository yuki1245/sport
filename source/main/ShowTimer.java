package sport.source.main;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Calendar;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.swing.JLabel;

class ShowTimer extends JLabel{
	private DateFormat format;

	ShowTimer(){
		this.setFont(new Font("Dialog", Font.BOLD, 24));
		format = new SimpleDateFormat("HH:mm:ss");
		Timer t = new Timer();
		t.schedule(new TTask(), 0, 1000);
	}

	public void setTime(){
		Calendar date1 = Calendar.getInstance();
		this.setText(format.format(date1.getTime()) + "\r");
	}

	class TTask extends TimerTask{
		public void run(){
			setTime();
		}
	}
}