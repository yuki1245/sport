import java.io.*;
import java.util.*;

public class Test1{
  private static String[][] array;

  public static void main(String[] args){
    try {
    File csv = new File("test.csv");
    FileInputStream input = new FileInputStream(csv);
    InputStreamReader stream = new InputStreamReader(input, "UTF-8");
    BufferedReader brf = new BufferedReader(stream);

    int loop = 0;
    ArrayList<String[]> al = new ArrayList<String[]>();

    while(brf.ready()) {
      String line = brf.readLine();
      al.add(line.split(","));
    }

    array = new String[al.size()][];
    for (int i = 0; i < al.size(); i++) {
      array[i] = al.get(i);
    }


    for (String[] str : array) {
      for (String element : str) {
        System.out.print(element);
      }
      System.out.println();
    }

    brf.close();

    } catch(FileNotFoundException e) {
     e.printStackTrace();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }
}