import javax.swing.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

public class Main extends JFrame{

  private String[][] tabledata = {
    {"日本", "3勝", "0敗", "1分"},
    {"クロアチア", "3勝", "1敗", "0分"},
    {"ブラジル", "1勝", "2敗", "1分"},
    {"ブラジル", "1勝", "2敗", "1分"},
    {"ブラジル", "1勝", "2敗", "1分"},
    {"ブラジル", "1勝", "2敗", "1分"},
    {"オーストラリア", "2勝", "2敗", "0分"}};

  private String[] columnNames = {"COUNTRY", "WIN", "LOST", "EVEN"};

  public static void main(String[] args){
    Main test = new Main("Main");

    test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    test.setVisible(true);
  }

  Main(String title){
    setTitle(title);
    setSize(500,500);

    JTable table = new JTable(tabledata, columnNames);

    JScrollPane sp = new JScrollPane(table);
    sp.setPreferredSize(new Dimension(400, 100));

    JPanel p = new JPanel();
    p.add(sp);

    add(p, BorderLayout.CENTER);
  }
}